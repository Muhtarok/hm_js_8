"use strict"

// 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
// Технічні вимоги:
// - Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, 
// другий аргумент - тип даних.
// - Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, 
// тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і 
// другим аргументом передати 'string', то функція поверне масив [23, null].


let arr = [1, "popa", true, null, undefined, "twar", false]; //new array declared
const filterBy = (arr, typeOfData) => {
    

    // for (let i = 0; i < 10; i++) {
    //     arr[i] = prompt(`Enter your array elements or "stop" for finish the array`, +i);
    //     if (arr[i] === "stop") {
    //         break;
    //     }
    // }

    console.log(arr);

    // typeOfData = prompt("Enter a type of Data");

    const result = arr.filter((word) =>  {  //in the new array there are only elements which are not equal to the name of the data type, provided in the second argument
        return typeof word !== typeOfData;
    });

    console.log(result);
}

console.log(filterBy(arr, "number"));


