"use strict"

// 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}. 
// Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча".



const arr = [{
    name: "Іван",
    age: 25,
    sex: "чоловіча",

},
{
    name: "Сашко",
    age: 14,
    sex: "чоловіча",
},
{
    name: "Анна",
    age: 20,
    sex: "жіноча",
},
{
    name: "Яна",
    age: 42,
    sex: "жіноча",
}]

let result = arr.filter((user) => {
    if (user.sex === "чоловіча") {
        return true;
    } else {
        return false;
    }
});

console.log(result);

// arr.forEach((person) => {
//     console.log(person);
// });




// function sayHi() {
//     return `Привіт, я ${this.name}, студент Dan, напрям ${this.educationDirection}`;
// };

// const students = [
//     {
//         name: "Bob",
//         surname: "Pip",
//         educationDirection: "Front-end",
//         sayHi,
//     },]

// students.forEach((student) => {
//     console.log(student.sayHi());
// });
